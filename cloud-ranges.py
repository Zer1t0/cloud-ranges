#!/usr/bin/env python3

from cloud_ranges.__main__ import main

if __name__ == '__main__':
    exit(main())
